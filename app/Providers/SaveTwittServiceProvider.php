<?php

namespace App\Providers;

use App\Helpers\SaveTwitt;
use Illuminate\Support\ServiceProvider;

class SaveTwittServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\Contracts\Save',function(){
            return new SaveTwitt();
        });
    }
}
