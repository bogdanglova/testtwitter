<?php

namespace App\Providers;

use App\Helpers\ConnectOAuther;
use Illuminate\Support\ServiceProvider;

class ConnectOAtherProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\Contracts\Connect',function(){
            return new ConnectOAuther();
        });
    }
}
