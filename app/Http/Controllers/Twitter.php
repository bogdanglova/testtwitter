<?php

namespace App\Http\Controllers;

use App\Helpers\SaveTwitt;
use Illuminate\Http\Request;
use App\Helpers\ConnectOAuther;
use Illuminate\Support\Facades\Session;


class Twitter extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $obj=new ConnectOAuther();
        $twitts=$obj->getData();
        Session::put('val',$twitts[0]->id);
        return view('twitts')->with('twitts',$twitts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,SaveTwitt $obj)
    {

        $obj->save($id);
         return $this->show();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $obj=new ConnectOAuther();
        $twitts=$obj->getData();
        if ($twitts[0]->id==Session::get('val')) {
            return 0;
        }
        else{
            return 1;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
