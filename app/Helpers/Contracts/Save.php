<?php
namespace App\Helpers\Contracts;
interface Save{
    public function save($id);
}