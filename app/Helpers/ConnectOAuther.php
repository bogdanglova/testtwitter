<?php
namespace App\Helpers;
use App\Helpers\Contracts\Connect;

use Abraham\TwitterOAuth\TwitterOAuth;

class ConnectOAuther implements Connect{
    private $connection,$data;
    function __construct()
    {
        $this->connection=new TwitterOAuth(env('CONSUMER_KEY'),env('CONSUMER_SECRET'),env('ACCESS_TOKEN'),env('ACCESS_TOKEN_SECRET'));
    }
    public function getData()
    {
        $this->data=$this->connection->get('statuses/user_timeline',['count'=>8,'screen_name'=>'UkrpravdaNews']);
        return $this->data;
    }
}