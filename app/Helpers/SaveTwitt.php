<?php
namespace App\Helpers;
use App\Helpers\Contracts\Save;
use Illuminate\Support\Facades\DB;

class SaveTwitt implements Save{
    private $result,$twitts;
    public function save($id)
    {
        $obj=new ConnectOAuther();
       foreach ($obj->getData() as $name=>$data){
           if ($id==$data->id){
               $this->twitts=DB::select("SELECT `twitt_id` FROM `twitts` ORDER BY `id` LIMIT 10");
               foreach ($this->twitts as $name=>$twitt){
                   if ($id==$twitt->twitt_id){
                       $this->result=1;
                   }
               }
               if (!$this->result){
                   DB::insert("INSERT INTO `twitts` (`username`,`twitt`,`twitt_time`,`twitt_id`) VALUES(?,?,?,?)",[$data->user->name,
                       $data->text,
                       $data->created_at,
                       $data->id]);
               }
           }
       }
       return $this->result;
    }
}