<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/twitt.css') }}" rel="stylesheet">
    <script
            src="http://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>
    <script src="{{ asset('js/reload_page.js') }}"></script>
    <title>Twitts List</title>
<body>
<div class="content">
<div class="heding">
    List of Twitts
</div>
    <div class="twitts">
        <table>

            <tr>
                <th>UserName</th>
                <th>Twitts</th>
                <th>DateTime</th>
                <th>Event</th>
            </tr>

            @foreach($twitts as $name=>$twit)
                <tr class="twitt">
                    <td>{{$twit->user->name}}</td>
                    <td>{{$twit->text}}</td>
                    <td>{{$twit->created_at}}</td>
                    <td>
                           <a href="/{{$twit->id}}"><button>Save</button></a>
                    </td>
                </tr>
            @endforeach

        </table>
    </div>
</div>
</body>
</html>

