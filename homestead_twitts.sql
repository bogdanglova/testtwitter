-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: homestead
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `twitts`
--

DROP TABLE IF EXISTS `twitts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitt_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitt_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `twitts`
--

LOCK TABLES `twitts` WRITE;
/*!40000 ALTER TABLE `twitts` DISABLE KEYS */;
INSERT INTO `twitts` VALUES (1,'Українська правда','Міжбанк відкрився зниженням гривні https://t.co/DrXTg4FlNR #новини #новости #Украина','Wed Aug 02 10:03:33 +0000 2017','892687304686489600'),(2,'Українська правда','\"Захід-2017\": Український посол упевнений, що РФ не нападе з Білорусі https://t.co/NbxvxGNfEj #новини #новости #Украина','Wed Aug 02 10:18:15 +0000 2017','892691003756478464'),(3,'Українська правда','Transparency International про справу на ЦПК: Тиск і переслідування https://t.co/N9Epox4GRp #новини #новости #Украина','Wed Aug 02 12:18:24 +0000 2017','892721242565988352'),(4,'Українська правда','Чотири країни приєдналися до продовження економічних санкцій проти Росії https://t.co/dtaf1RKkvx #новини #новости #Украина','Wed Aug 02 13:03:26 +0000 2017','892732574153486336');
/*!40000 ALTER TABLE `twitts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-02 18:03:06
